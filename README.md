The app has been written using Electron, Node.js, Vue.js.

The app can parse proxies from different sites. And it can open specified page in different windows through different proxies. Also, you can import proxies from a file.
 
To run the app use command:
```
npm run-script buildAndRun
```
