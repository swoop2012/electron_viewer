module.exports = {
    getTimeout(ms, cb) {
        return new Promise(resolve => setTimeout(() => {
            if (typeof cb === 'function') {
                cb()
            }
            resolve()
        }, ms));
    }
}