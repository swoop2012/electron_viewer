//include Vue and home component
var ECharts = require('vue-echarts');
var Vue = require('vue');
var Home = require('./components/home.vue');
var VueResource = require('vue-resource');
var Datetime = require('vue-datetime');
const {VueSelect} = require('vue-select')
const bVue = require('bootstrap-vue')

Vue.component('datetime', Datetime.Datetime);
Vue.component('v-select', VueSelect)
Vue.use(bVue)
Vue.use(VueResource);

//instantiate Vue
new Vue({
    el: '#app',
    //render function is used to render the component
    render: function (createElement) {
        return createElement(Home);
    }
});
