const electron = require('electron')
// Module to control application life.
const app = electron.app
// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow

const path = require('path')
const url = require('url')
const fs = require('fs')

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow
function createWindow() {
    // Create the browser window.
    mainWindow = new BrowserWindow()
    mainWindow.maximize()
    // and load the index.html of the app.

    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'index.html'),
        protocol: 'file:',
        slashes: true
    }))

    // Emitted when the window is closed.
    mainWindow.on('closed', function () {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        mainWindow = null
    })

    mainWindow.webContents.on('new-window', function (event, url) {

        event.preventDefault();
        electron.shell.openExternal(url);
    });

}
let timeouts = {}, cbs = {};
function loadUrlWithProxy(win, url, proxies, event) {
    if(proxies.length) {
        let proxy = proxies.shift()
        // win.webContents.session.clearStorageData([], (data) => {})
        win.webContents.session.setProxy({proxyRules:`http://${proxy}`}, function () {

            win.loadURL(url)
        });
        event.sender.send('proxy-used', proxy)
    }
    else {
        win.close()
    }
}
electron.ipcMain.on('ajax-requests',  (event, args) => {
    console.log("ajax-requests", args)
    if(timeouts.hasOwnProperty(args.id) && timeouts[args.id]) {
        clearTimeout(timeouts[args.id])
        cbs[args.id]()
    }

})

electron.ipcMain.on('load-url-with-proxy',  (event, args) => {
    let threads = parseInt(args.threads)
    let step = Math.ceil(args.proxies.length / threads);
    for(let i = 0; i < args.proxies.length; i+= step){
        let slice = args.proxies.slice(i, i + step)
        let win = new BrowserWindow({width:1000, height:800,webPreferences: {
            preload: path.resolve(__dirname, 'src/js/preload.js')
        },});
        let id = i + step;
        win.webContents.on('did-finish-load',()=>{
            win.webContents.executeJavaScript(`
            const electron = window.require('electron')
            const ipcRenderer = electron.ipcRenderer
            if(typeof $ === 'function') {
                $(document).ajaxComplete(function(event, xhr, options){
                    if('url' in options && options.url.includes('/v2/projects/71578381/view')){
                        ipcRenderer.send('ajax-requests', {id:${id}})
                    }
                });
            }
        `, true)
            cbs[id] = ()=>{
                if(!win.isDestroyed()) {
                    loadUrlWithProxy(win, args.url, slice, event)
                }
            }
            timeouts[id] = setTimeout(cbs[id], 6000)
        })
        loadUrlWithProxy(win, args.url, slice, event)
    }
})
electron.ipcMain.on('open-file', (event, path) => {
    const fs = require('fs')
    electron.dialog.showOpenDialog(function (fileNames) {
        // fileNames is an array that contains all the selected
        if(fileNames === undefined) {
            console.log("No file selected");
        } else {
            for(let key in fileNames) {
                readFile(fileNames[key]);
            }
        }
    });

    function readFile(filepath) {
        fs.readFile(filepath, 'utf-8', (err, data) => {

            if(err){
                alert("An error ocurred reading the file :" + err.message)
                return
            }
            let lines = data.split("\n");
            let regex = /.*\]\s(.*)>/
            for(let i = 0; i < lines.length; i++){
                if(lines[i].match(regex)) {
                    lines[i] = lines[i].replace(regex, '$1')
                }
            }

            // handle the file content
            event.sender.send('loaded-proxies', lines)
        })
    }
})
// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function () {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit()
    }
})

app.on('activate', function () {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (mainWindow === null) {
        createWindow()
    }
})
